var express = require('express');
var router = express.Router();
const request = require('request');
//require('request-debug')(request);

const topicDetectionUrl = "https://mlfproduction-topic-detection.cfapps.eu10.hana.ondemand.com/api/v2/text/topic-detection"

router.post('/topic-detection', function (req, res, next) {
    
    //Get the authentication token for the machine learning services    
    let token = getToken(function(token, error) {
        if (token) {

            //Forward the request to the machinelearning service using the token
            forwardRequest(req, res, token)
        }
    });
});

function getToken(tokenCallback) {

    var form = {}
    form["grant_type"] = "client_credentials"

    var username = "sb-5c1654ae-1d49-4985-b760-cdb388828c6e!b6830|ml-foundation-xsuaa-std!b540"
    var password = "C+vAbPJNLQATu/hJU0tXNL2x0YU="

    request.post('https://sapconnect.authentication.eu10.hana.ondemand.com/oauth/token', { json: true, form: form }, (err, res, body) => {
        if (err) {
            console.log(err)
            tokenCallback(null, error)
        }
        else {
            tokenCallback(body.access_token, null)
        }
    }).auth(username, password);
}

function forwardRequest(req, res, token) {
    delete req.headers.host

    req.headers["content-type"] = "application/x-www-form-urlencoded"

    console.log("Request form: " + JSON.stringify(req.body))
    
    request.post(topicDetectionUrl, {useQuerystring: true, form: req.body, headers: req.headers}, (error, response, body) => {
        if (error) {
            return console.log(error)
        }

        console.log("Response from ml: " + body)
        
        for (var header in response.headers) {
            res.set(header, response.headers[header])
        }

        res.write(body)
        res.end()
    }).auth(null, null, true, token)

}

module.exports = router;
